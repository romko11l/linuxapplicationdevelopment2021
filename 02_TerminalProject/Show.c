#include <stdio.h>
#include <ncurses.h>
#include <string.h>
#include <stdlib.h>

// very long string 12345678912345678912345678912345678923456789123456789123456789123456789123456789123456789123456789234567891234567891234567891234567891234567891234567891234567892345678912345678912345678912345678912345678912345678912345678923456789123456789123456789

#define DX 3

bool file_is_over = false;

void write_line_from_file(FILE *f, int win_width, WINDOW *win) {
    char small_line[win_width-3];
    char *line = NULL;
    size_t len = 0;
    ssize_t read;

    if ((read = getline(&line, &len, f)) != -1) {
        if (strlen(line) > win_width) {
            for (int i = 0; i < (win_width-3); i++) {
                small_line[i] = line[i];
            }
            wprintw(win, " %s\n", small_line);
        } else {
            wprintw(win, " %s", line);
        }
    } else {
        if (!file_is_over) {
            file_is_over = true;
            wprintw(win, " \n");
        }
    }
    free(line);
}

void print_file(FILE *f, const char *filename) {
    WINDOW *win;
    int c = 0;

    initscr();
    noecho();
    cbreak();

    printw("File: %s", filename);
    refresh();

    win = newwin(LINES-2*DX, COLS-2*DX, DX, DX);
    keypad(win, TRUE);
    scrollok(win, TRUE);
    
    int win_height = LINES-2*DX;
    int win_width = COLS-2*DX;

    wprintw(win, "\n");
    box(win, 0, 0);
    wrefresh(win);

    for (int i = 0; i < win_height - 2; i++) {
        write_line_from_file(f, win_width, win);
    }
    box(win, 0, 0);
    wrefresh(win);

    c = wgetch(win);
    while (c != 27) {
        if (c == ' ') {
            write_line_from_file(f, win_width, win);
            box(win, 0, 0);
            wrefresh(win);
        }
        c = wgetch(win);
    }

    endwin();
}


// another long string 12345678912345678912345678912345678923456789123456789123456789123456789123456789123456789123456789234567891234567891234567891234567891234567891234567891234567892345678912345678912345678912345678912345678912345678912345678923456789123456789123456789

int main(int argc, char *argv[]) {
    if (argc == 1) {
        printf("Required filename\n");
        return 1;
    } else if (argc == 2) {
        FILE *f = fopen(argv[1], "r");
        if (f == NULL) {
            printf("File \"%s\" not found\n", argv[1]);
            return 1;
        }

        print_file(f, argv[1]);

        fclose(f);

        return 0;
    } else {
        printf("Required only one filename\n");
        return 1;
    }
}
